import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

const String _url = "https://courses.fluttermapp.com/p/zero-to-hero";

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<XFile>? _imageFileList;
  final ImagePicker _picker = ImagePicker();
  VideoPlayerController? _toBeDisposed;
  VideoPlayerController? _videoController;
  bool isVideo = false;

  Future<void> _playVideo(XFile? file) async {
    if (file != null) {
      await _disposeVideoController();
      late VideoPlayerController videoController;

      videoController = VideoPlayerController.file(File(file.path));
      _videoController = videoController;

      await videoController.initialize();
      await videoController.setLooping(true);
      await videoController.play();
      setState(() {});
    }
  }

  Future<void> _disposeVideoController() async {
    if (_toBeDisposed != null) {
      await _toBeDisposed!.dispose();
    }
    _toBeDisposed = _videoController;
    _videoController = null;
  }

  void _launchURL() async {
    if (!await launch(_url)) throw 'Could not launch $_url';
  }

  void _getImageAndVideoPicker(
    ImageSource source, {
    BuildContext? context,
    bool isMultiple = false,
  }) async {
    try {
      if (isVideo) {
        final XFile? file = await _picker.pickVideo(source: source);
        await _playVideo(file);
      } else if (isMultiple) {
        final pickedFileList = await _picker.pickMultiImage();
        setState(() {
          _imageFileList = pickedFileList;
        });
      } else {
        final pickedFile = await _picker.pickImage(source: source);
        setState(() {
          _imageFileList = pickedFile == null ? null : [pickedFile];
        });
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Widget _previewImages() {
    try {
      if (_imageFileList != null) {
        return ListView.builder(
          itemBuilder: (context, index) {
            return Image.file(File(_imageFileList![index].path));
          },
          itemCount: _imageFileList!.length,
        );
      }
      else {
        return const Text("You need to pick an image");
      }
    } catch (e) {
      String errorMsg = e.toString();

      print(errorMsg);
      return Text(errorMsg);
    }
  }

  Widget _previewVideo() {
    try {
      if (_videoController != null) {
        // return VideoPlayer(_videoController!);
        return AspectRatio(
          child: VideoPlayer(_videoController!),
          aspectRatio: _videoController!.value.aspectRatio,
        );
      } else {
        return const Text("Pick a video");
      }
    } catch (e) {
      String errorMsg = e.toString();

      print(errorMsg);
      return Text(errorMsg);
    }
  }

  @override
  void dispose() {
    _disposeVideoController();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Packages"),
        ),
        body: Center(
          child: isVideo ? _previewVideo() : _previewImages(),
        ),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: FloatingActionButton(
                onPressed: () {
                  isVideo = false;

                  _getImageAndVideoPicker(
                    ImageSource.gallery,
                    context: context,
                    isMultiple: true,
                  );
                },
                child: const Icon(Icons.photo_library_sharp),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: FloatingActionButton(
                onPressed: () {
                  isVideo = false;

                  _getImageAndVideoPicker(
                    ImageSource.gallery,
                    context: context,
                  );
                },
                child: const Icon(Icons.image_sharp),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: FloatingActionButton(
                onPressed: () {
                  isVideo = true;

                  _getImageAndVideoPicker(
                    ImageSource.gallery,
                    context: context,
                  );
                },
                child: const Icon(Icons.video_library_sharp),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: FloatingActionButton(
                onPressed: () {
                  isVideo = true;

                  _getImageAndVideoPicker(
                    ImageSource.camera,
                    context: context,
                  );
                },
                child: const Icon(Icons.videocam_sharp),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: FloatingActionButton(
                onPressed: () {
                  isVideo = false;

                  _getImageAndVideoPicker(
                    ImageSource.camera,
                    context: context,
                  );
                },
                child: const Icon(Icons.camera_sharp),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: FloatingActionButton(
                onPressed: () => _launchURL(),
                child: const Icon(Icons.network_wifi_sharp),
              ),
            ),
          ],
        ));
  }
}
